package tests;

import checkout.Reduction;
import checkout.rules.BuyGetFreeRule;
import checkout.rules.Rule;
import trading.Item;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReductionTest {
	
	
	@Test
	public void testReduction() {
		Item item = new Item(1, "Tomatos", 0.75);
		Rule onePlusOne = new BuyGetFreeRule("1+1", 1, 1);
		Reduction reduction1 = new Reduction(onePlusOne, item, 0.375);
		assertEquals(item, reduction1.getItem());
		assertEquals(onePlusOne, reduction1.getRule());
		assertEquals(0.375, reduction1.getSum(), 0);
		try {
			Reduction reduction2 = new Reduction(null, item, 0.375);
			assertTrue(false);	
		} catch(IllegalArgumentException e) {}
		try {
			Reduction reduction3 = new Reduction(onePlusOne, null, 0.375);
			assertTrue(false);	
		} catch(IllegalArgumentException e) {}
		try {
			Reduction reduction4 = new Reduction(null, null, 0.375);
			assertTrue(false);	
		} catch(IllegalArgumentException e) {}
	}
}
