package tests;

import static org.junit.Assert.*;

import org.junit.*;

import trading.Item;

public class ItemTest {
	
	@Test
	public void testItem() {
		long id = 123;
		String name = "Bread";
		double price = 1.75;
		double illegalPrice = -0.5;
		Item item1 = new Item(id, name, price);
		assertEquals(id, item1.getId());
		assertEquals(name, item1.getName());
		Assert.assertEquals(price, item1.getPrice(),0);
		Item item2;
		try {
			item2 = new Item(id, name, illegalPrice);
			assertTrue(false);
		} catch(IllegalArgumentException e) {		
		}	
		try {
			item1.setPrice(illegalPrice);
			assertTrue(false);
		} catch(IllegalArgumentException e) {
		}	
	}
}
