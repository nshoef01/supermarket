package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import trading.Basket;
import trading.Item;

public class BasketTest {
	
	
	@Test
	public void testBasket() {
		long id = 123;
		Basket basket = new Basket(id);
		assertEquals(id, basket.getId());
		assertEquals(0, basket.getItems().size());
	}
	
	@Test
	public void addItem() {
		Item item1 = new Item(1, "Lager", 4.0);
		Item item2 = new Item(2, "Ale", 4.5);
		
		long id = 123;
		Basket basket = new Basket(id);
		
		basket.addItem(item1, 1);
		assertEquals(1, basket.getItems().size());
		assertTrue(basket.getItems().keySet().contains(item1));
		assertEquals(1, basket.getItems().get(item1).intValue());
		basket.addItem(item2, 3);
		assertEquals(2, basket.getItems().size());
		assertTrue(basket.getItems().keySet().contains(item1));
		assertTrue(basket.getItems().keySet().contains(item2));
		assertEquals(1, basket.getItems().get(item1).intValue());
		assertEquals(3, basket.getItems().get(item2).intValue());
		basket.addItem(item1, 2);
		assertTrue(basket.getItems().keySet().contains(item1));
		assertTrue(basket.getItems().keySet().contains(item2));
		assertEquals(3, basket.getItems().get(item1).intValue());
		assertEquals(3, basket.getItems().get(item2).intValue());

		try{
			basket.addItem(null, 0);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try{
			basket.addItem(item1, -1);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try{
			basket.addItem(null, 1);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
	}
	 
	@Test
	public void removeItem() {
		Item item1 = new Item(1, "Lager", 4.0);
		Item item2 = new Item(2, "Ale", 4.5);
		
		long id = 123;
		Basket basket = new Basket(id);
		basket.addItem(item1, 2);
		basket.removeItem(item1, 1);
		assertEquals(1, basket.getItems().get(item1).intValue());
		basket.addItem(item2, 6);
		basket.removeItem(item2, 2);
		assertEquals(1, basket.getItems().get(item1).intValue());
		assertEquals(4, basket.getItems().get(item2).intValue());
		basket.removeItem(item1, 1);
		assertFalse(basket.getItems().keySet().contains(item1));
		
		try {
			basket.removeItem(item2, 0);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			basket.removeItem(null, 2);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			basket.removeItem(null, -2);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
	}
}
