package tests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import checkout.Receip;
import checkout.Reduction;
import checkout.rules.BuyGetFreeRule;
import checkout.rules.CheapestFromRule;
import checkout.rules.NForSpecialRule;
import checkout.rules.Rule;
import trading.Basket;
import trading.Item;

public class ReceipTest {
	
	
	@Test
	public void testReceip() {
        Item i1 = new Item(1, "item1", 2.5);
        Item i2 = new Item(2, "item2", 1.0);
        Item i3 = new Item(3, "item3", 2.5);
        Item i4 = new Item(4, "item4", 10.0);
        Item i5 = new Item(5, "item5", 0.5);
        
        Rule r1 = new BuyGetFreeRule("3for2", 2, 1);
        Rule r2 = new NForSpecialRule("10% discount on all items", 1, 10.0);
        Rule r3 = new CheapestFromRule("Buy 3 items get 1 free", 3, 1);
		
        r1.addItem(i1);
        r1.addItem(i2);
        
        r2.addItem(i1);
        r2.addItem(i2);
        r2.addItem(i3);
        r2.addItem(i4);
        r2.addItem(i5);
        
        r3.addItem(i4);
        r3.addItem(i5);
        
		Basket basket1 = new Basket(1);
		Basket basket2 = new Basket(2);
		
		basket1.addItem(i1, 2); // 2.5x2=5
		basket1.addItem(i3, 5); // 2.5x5=12.5
		basket1.addItem(i5, 4); // 0.5x4=2
		
		basket2.addItem(i2, 3); // 1x3=3
		basket2.addItem(i4, 1); // 10x1=10
		basket2.addItem(i5, 10); // 0.5x10=5 
		
		Set<Rule> set1 = new HashSet<>();
		set1.add(r1);
		set1.add(r2);
		set1.add(r3);
		
		Set<Rule> set2 = new HashSet<>();
		set2.add(r2);
		
		Receip receip1 = new Receip(basket1, set1);
		assertEquals(basket1, receip1.getBasket());
		List<Reduction> reduction1 = receip1.getReductions();
		assertEquals(0.5, reduction1.get(0).getSum(), 0);
		assertEquals(i1, reduction1.get(0).getItem());
		assertEquals(r2, reduction1.get(0).getRule());
		
		assertEquals(0.5, reduction1.get(3).getSum(), 0);
		assertEquals(i5, reduction1.get(3).getItem());
		assertEquals(r3, reduction1.get(3).getRule());
		
		assertEquals(4, reduction1.size());
		assertEquals(19.5, receip1.getTotalBeforeReduction(), 0);
		assertEquals(17.05, receip1.getTotalAfterReduction(), 0);
		
		Receip receip2 = new Receip(basket1, set2);
		assertEquals(basket1, receip1.getBasket());
		List<Reduction> reduction2 = receip2.getReductions();
		assertEquals(3, reduction2.size());
		assertEquals(1.25, reduction2.get(1).getSum(), 0);
		assertEquals(i3, reduction2.get(1).getItem());
		assertEquals(r2, reduction2.get(1).getRule());
		assertEquals(19.5, receip2.getTotalBeforeReduction(), 0);
		assertEquals(17.55, receip2.getTotalAfterReduction(), 0);
		
		Receip receip3 = new Receip(basket2, set1);
		assertEquals(basket2, receip3.getBasket());
		List<Reduction> reduction3 = receip3.getReductions();
		assertEquals(6, reduction3.size());
		assertEquals(0.5, reduction3.get(5).getSum(), 0);
		assertEquals(i5, reduction3.get(5).getItem());
		assertEquals(r3, reduction3.get(5).getRule());
		assertEquals(18.0, receip3.getTotalBeforeReduction(), 0);
		assertEquals(14.2, receip3.getTotalAfterReduction(), 0);
			
	}
}
