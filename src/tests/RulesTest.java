package tests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import checkout.Reduction;
import checkout.rules.BuyGetFreeRule;
import checkout.rules.CheapestFromRule;
import checkout.rules.GetFreeItemRule;
import checkout.rules.NForSpecialRule;
import checkout.rules.Rule;
import trading.Basket;
import trading.Item;

public class RulesTest {
	
	@Test
	public void testBuyGetFreeRule() {
        Item i1 = new Item(1, "item1", 2.5);
        Item i2 = new Item(2, "item2", 1.0);
        Item i3 = new Item(3, "item3", 2.5);
        Item i4 = new Item(4, "item4", 10.0);
        
		Basket basket1 = new Basket(1);
		Basket basket2 = new Basket(2);
		
		basket1.addItem(i1, 3); // 2.5x3=7.5
		basket1.addItem(i2, 1); // 1x1=1
		basket1.addItem(i3, 4); // 2.5x4=10
		
		basket2.addItem(i2, 4); // 1x4=4
		basket2.addItem(i3, 9); // 2.5x9=22.5
		basket2.addItem(i4, 6); // 10x6=60 
		
		Rule rule;
		try {
			rule = new BuyGetFreeRule("name", 0, 1);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new BuyGetFreeRule("name", 2, 0);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		
		rule = new BuyGetFreeRule("1 + 1", 1, 1);
		assertEquals("1 + 1", rule.getName());
		rule.addItem(i1);
		rule.addItem(i2);
		
		assertTrue(rule.getItems().contains(i1));
		assertFalse(rule.getItems().contains(i3));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction1 = rule.applyOn(basket1);
		assertEquals(1, reduction1.size());
		assertEquals(2.5, reduction1.get(0).getSum(), 0);
		assertEquals(i1, reduction1.get(0).getItem());
		assertEquals(rule, reduction1.get(0).getRule());
		
		rule.addItem(i3);
		rule.addItem(i4);
		List<Reduction> reduction2 = rule.applyOn(basket1);
		assertEquals(2, reduction2.size());
		assertEquals(5, reduction2.get(1).getSum(), 0);
		assertEquals(i3, reduction2.get(1).getItem());
		assertEquals(rule, reduction2.get(1).getRule());
		
		rule = new BuyGetFreeRule("Buy 3 get 2", 3, 2);
		Set<Item> items = new HashSet<>();
		items.add(i3);
		items.add(i4);
		rule.addItems(items);
		assertTrue(rule.getItems().contains(i3));
		assertFalse(rule.getItems().contains(i1));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction3 = rule.applyOn(basket2);
		assertEquals(2, reduction3.size());
		assertEquals(7.5, reduction3.get(1).getSum(), 0);
		assertEquals(i3, reduction3.get(1).getItem());
		assertEquals(rule, reduction3.get(1).getRule());
		assertEquals(20, reduction3.get(0).getSum(), 0);
		assertEquals(i4, reduction3.get(0).getItem());
		assertEquals(rule, reduction3.get(0).getRule());	
	}
	
	@Test
	public void testCheapestFromRule() {
        Item i1 = new Item(1, "item1", 2.5);
        Item i2 = new Item(2, "item2", 1.0);
        Item i3 = new Item(3, "item3", 2.5);
        Item i4 = new Item(4, "item4", 10.0);
        
		Basket basket1 = new Basket(1);
		Basket basket2 = new Basket(2);
		
		basket1.addItem(i1, 3); // 2.5x3=7.5
		basket1.addItem(i2, 1); // 1x1=1
		basket1.addItem(i3, 4); // 2.5x4=10
		
		basket2.addItem(i2, 4); // 1x4=4
		basket2.addItem(i3, 9); // 2.5x9=22.5
		basket2.addItem(i4, 6); // 10x6=60 
		
		Rule rule;
		try {
			rule = new CheapestFromRule("name", 0, 1);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new CheapestFromRule("name", 2, 0);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		
		rule = new CheapestFromRule("Buy any 2 items get the cheapest for free", 2, 1);
		assertEquals("Buy any 2 items get the cheapest for free", rule.getName());
		rule.addItem(i1);
		rule.addItem(i2);
		
		assertTrue(rule.getItems().contains(i1));
		assertFalse(rule.getItems().contains(i3));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction1 = rule.applyOn(basket1);
		assertEquals(2, reduction1.size());
		assertEquals(2.5, reduction1.get(0).getSum(), 0);
		assertEquals(i1, reduction1.get(0).getItem());
		assertEquals(rule, reduction1.get(0).getRule());
		
		rule.addItem(i3);
		rule.addItem(i4);
		List<Reduction> reduction2 = rule.applyOn(basket1);
		assertEquals(4, reduction2.size());
		assertEquals(1, reduction2.get(3).getSum(), 0);
		assertEquals(i2, reduction2.get(3).getItem());
		assertEquals(rule, reduction2.get(3).getRule());
		
		rule = new CheapestFromRule("Buy any 5 items get the 2 cheapest for free", 5, 2);
	
		rule.addItem(i2);
		rule.addItem(i3);
		rule.addItem(i4);
		assertTrue(rule.getItems().contains(i3));
		assertFalse(rule.getItems().contains(i1));
		assertEquals(3, rule.getItems().size());
		List<Reduction> reduction3 = rule.applyOn(basket2);
		assertEquals(6, reduction3.size());
		assertEquals(10, reduction3.get(0).getSum(), 0);
		assertEquals(i4, reduction3.get(0).getItem());
		assertEquals(rule, reduction3.get(0).getRule());
		assertEquals(2.5, reduction3.get(5).getSum(), 0);
		assertEquals(i3, reduction3.get(5).getItem());
		assertEquals(rule, reduction3.get(5).getRule());	
	}
	
	@Test
	public void testNForSpecialRule() {
        Item i1 = new Item(1, "item1", 2.5);
        Item i2 = new Item(2, "item2", 1.0);
        Item i3 = new Item(3, "item3", 2.5);
        Item i4 = new Item(4, "item4", 10.0);
        
		Basket basket1 = new Basket(1);
		Basket basket2 = new Basket(2);
		
		basket1.addItem(i1, 3); // 2.5x3=7.5
		basket1.addItem(i2, 1); // 1x1=1
		basket1.addItem(i3, 4); // 2.5x4=10
		
		basket2.addItem(i2, 4); // 1x4=4
		basket2.addItem(i3, 9); // 2.5x9=22.5
		basket2.addItem(i4, 6); // 10x6=60 
		
		Rule rule;
		try {
			rule = new NForSpecialRule("name", 0, 45);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new NForSpecialRule("name", 2, -1);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new NForSpecialRule("name", 2, 101);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		
		rule = new NForSpecialRule("20% off", 1, 20);
		assertEquals("20% off", rule.getName());
		rule.addItem(i1);
		rule.addItem(i2);
		
		assertTrue(rule.getItems().contains(i1));
		assertFalse(rule.getItems().contains(i3));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction1 = rule.applyOn(basket1);
		assertEquals(2, reduction1.size());
		assertEquals(1.5, reduction1.get(0).getSum(), 0);
		assertEquals(i1, reduction1.get(0).getItem());
		assertEquals(rule, reduction1.get(0).getRule());
		
		rule.addItem(i3);
		rule.addItem(i4);
		List<Reduction> reduction2 = rule.applyOn(basket1);
		assertEquals(3, reduction2.size());
		assertEquals(2, reduction2.get(2).getSum(), 0);
		assertEquals(i3, reduction2.get(2).getItem());
		assertEquals(rule, reduction2.get(2).getRule());
		
		rule = new NForSpecialRule("Second item in 50%", 2, 25);
		Set<Item> items = new HashSet<>();
		items.add(i3);
		items.add(i4);
		rule.addItems(items);
		assertTrue(rule.getItems().contains(i3));
		assertFalse(rule.getItems().contains(i1));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction3 = rule.applyOn(basket2);
		assertEquals(2, reduction3.size());
		assertEquals(15, reduction3.get(1).getSum(), 0);
		assertEquals(i4, reduction3.get(1).getItem());
		assertEquals(rule, reduction3.get(1).getRule());
		assertEquals(5, reduction3.get(0).getSum(), 0);
		assertEquals(i3, reduction3.get(0).getItem());
		assertEquals(rule, reduction3.get(0).getRule());
	}
	
	@Test
	public void testGetFreeItemRuleRule() {
        Item i1 = new Item(1, "item1", 2.5);
        Item i2 = new Item(2, "item2", 1.0);
        Item i3 = new Item(3, "item3", 2.5);
        Item i4 = new Item(4, "item4", 1.0);
        
		Basket basket1 = new Basket(1);
		Basket basket2 = new Basket(2);
		
		basket1.addItem(i1, 3); // 2.5x3=7.5
		basket1.addItem(i2, 1); // 1x1=1
		
		basket2.addItem(i3, 2); // 2.5x9=22.5
		basket2.addItem(i4, 6); // 10x6=60 
		
		Rule rule;
		try {
			rule = new GetFreeItemRule("name", 0, 2, i4);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new GetFreeItemRule("name", 1, 0, i4);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		try {
			rule = new GetFreeItemRule("name", 2, 1, null);
			assertTrue(false);
		} catch(IllegalArgumentException e) {}
		
		rule = new GetFreeItemRule("Buy 2 i3 get 2 i4 for free", 2, 2, i4);
		assertEquals("Buy 2 i3 get 2 i4 for free", rule.getName());
		rule.addItem(i3);
		
		assertTrue(rule.getItems().contains(i3));
		assertFalse(rule.getItems().contains(i4));
		assertEquals(1, rule.getItems().size());
		List<Reduction> reduction1 = rule.applyOn(basket1);
		assertEquals(0, reduction1.size());
		
		basket1.addItem(i3, 7);
		
		List<Reduction> reduction2 = rule.applyOn(basket1);
		assertEquals(6, reduction2.size());
		assertEquals(1, reduction2.get(2).getSum(), 0);
		assertEquals(i4, reduction2.get(2).getItem());
		assertEquals(rule, reduction2.get(2).getRule());
		
		rule = new GetFreeItemRule("Buy i3 or i4 get i1 for free", 1, 1, i1);
		Set<Item> items = new HashSet<>();
		items.add(i3);
		items.add(i4);
		rule.addItems(items);
		assertTrue(rule.getItems().contains(i3));
		assertFalse(rule.getItems().contains(i1));
		assertEquals(2, rule.getItems().size());
		List<Reduction> reduction3 = rule.applyOn(basket2);
		assertEquals(8, reduction3.size());
		assertEquals(2.5, reduction3.get(0).getSum(), 0);
		assertEquals(i1, reduction3.get(0).getItem());
		assertEquals(rule, reduction3.get(0).getRule());
		assertEquals(2.5, reduction3.get(7).getSum(), 0);
		assertEquals(i1, reduction3.get(7).getItem());
		assertEquals(rule, reduction3.get(7).getRule());
	}

}
