package trading;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nshoef on 02/06/2017.
 * Basket is a collection of items choosen by the customer to be bought in a single transaction.
 * It contain a uniqu id to identify it and a map of required items and their required quantity.
 */
public class Basket {
    private final Long ID;
    private final Map<Item, Integer> items = new HashMap<>();


    public Basket(long id) {
        this.ID = id;
    }

    /**
     * Add an item to the basket.
     * @param item a nun null item to add to the basket.
     * @param quantity the number of this items to be added. must be a positive number.
     */
    public void addItem(Item item, int quantity) {
        if(item==null) throw new IllegalArgumentException("can not add null items");
        if(quantity<1) throw new IllegalArgumentException("quantity must be greater then 0");
        if(items.containsKey(item)) {
            int newQuantity = items.get(item)+quantity;
            items.replace(item, newQuantity);
        } else {
            items.put(item, quantity);
        }
    }

    /**
     * Removes a quantity sum of item items from the basket
     * @param item a nun null item from the basket's items to be removed.
     * @param quantity a positive quantity of this item to be removed (must be between 1 to the quantity of the item in the basket)
     */
    public void removeItem(Item item, int quantity) {
        if(item == null) throw new IllegalArgumentException("item can not be null");
        if(quantity<1) throw new IllegalArgumentException("quantity must be greater then 0");
        if(!items.containsKey(item)) throw new IllegalArgumentException("Item not in basket");
        if(items.get(item) < quantity) throw new IllegalArgumentException("quantity is higher them in basket");
        int newQuantity = items.get(item) - quantity;
        if(newQuantity == 0) {
            items.remove(item);
        } else {
            items.replace(item, newQuantity);
        }    
    }

    public Map<Item, Integer> getItems() {
        return items;
    }

    public long getId(){
        return ID;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Basket: "+ID+", items: ");
        for(Item item : items.keySet()) {
            sb.append("["+item.getName()+": "+item.getPrice()+"X"+items.get(item)+"]");
        }
        return sb.toString();
    }

    public int hashCode() {
        int hashValue = 7;
        hashValue = 31 * hashValue+Long.hashCode(ID);
        hashValue = 31 * hashValue+items.hashCode();
        return hashValue;
    }
}
