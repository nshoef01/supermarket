package trading;

/**
 * Created by nshoef on 02/06/2017.
 * Item is an object which represent an item for sale. It has a unique identifier, a descriptive name and a positive price.
 * The name and the price can be changed;
 */
public class Item {
    private final long ID;
    private String name;
    private double price;


    public Item(long id, String name, double price) {
        if(price<0) throw new IllegalArgumentException("price must be positive");
        this.ID = id;
        this.name = name;
        this.price = price;
    }

    public long getId() { return ID;}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPrice(double price) {
        if(price<0) throw new IllegalArgumentException("price must be positive");
        this.price = price; }

    public double getPrice() {
        return price;
    }

    public String toString() {
        return "Item[ID: "+ID+", name: "+name+", price: "+price+"]";
    }

    public int hashcode() {
        int hashValue = 7;
        hashValue = 37 * hashValue + Long.hashCode(ID);
        hashValue = 37 * hashValue + name.hashCode();
        hashValue = 37 * hashValue + Double.hashCode(price);
        return hashValue;
    }
}
