package operation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import checkout.CheckoutService;
import checkout.Receip;
import checkout.rules.BuyGetFreeRule;
import checkout.rules.CheapestFromRule;
import checkout.rules.GetFreeItemRule;
import checkout.rules.NForSpecialRule;
import checkout.rules.Rule;
import trading.Basket;
import trading.Item;

/**
 * This is a simple implementation of a supermarket which demonstrate the use of checkout service
 * @author nshoef
 *
 */
public class SupermarketOperator {
	private Map<Long, Item> items = new HashMap<>(); 
	private CheckoutService checkout = new CheckoutService();
	private Set<Rule> rules = new HashSet<>();
	Scanner scan;
	
	public static void main(String[] agrs) {
		SupermarketOperator so = new SupermarketOperator();
		so.run();
	}
	
	
	public void run() {
		scan = new Scanner(System.in);
		prepareSupermarket();
		openSupermarket();	
	}
	
	/**
	 * this method runs a single transaction of the supermarket
	 */
	private void openSupermarket() {
		
		long choice = -1; 
		System.out.println("Welcome to the supermarket/n");
		
		Basket basket = new Basket(1);
		while(choice != 0) {
			System.out.println("Choose item (id) from the list or 0 to checkout:");
			items.forEach((id, item) -> System.out.println(item));
			choice = getUserChoice();
			
			if(items.containsKey(choice)) {
				System.out.println("Enter quantity:");
				int quantity = getUserQuantity();
				System.out.println("adding " + quantity +" X "+items.get(choice)+"\n");
				basket.addItem(items.get(choice), quantity);
				System.out.println("MyBasket:" + basket);
				System.out.println();
		
			} else {
				if(choice == 0) {
					scan.close();
					System.out.println("Cheching out: "+basket);
					System.out.println();
					Receip receip = checkout.checkout(basket);
					System.out.println("Receip for "+receip.getBasket().getId());
					System.out.println("===================");
					receip.getBasket().getItems().forEach((x, y) -> System.out.println(x+"x"+y));
					System.out.println("TOTAL before reductions: "+receip.getTotalBeforeReduction());
					System.out.println("----Reductions-----");
					receip.getReductions().forEach(x -> System.out.println(x));
					System.out.println();
					System.out.println("TOTAL to pay: "+receip.getTotalAfterReduction());		
				} else {
					System.out.println("Item not exisit");
				}
			}
		}
	}
		
	private long getUserChoice() {
		long result = -1;
		while(true) {
			String input = scan.nextLine();
			try {
				result = Long.parseLong(input);
				if(result < 0) {
					System.out.println("Illegal input");
				} else {
					return result;
				}
			} catch(NumberFormatException e) {
				System.out.println("Illegal input");
			}
		}
	}
	
	private int getUserQuantity() {
		int result = -1;
		while(true) {
			String input = scan.nextLine();
			try {
				result = Integer.parseInt(input);
				if(result < 1) {
					System.out.println("Illegal input");
				} else {
					return result;
				}
			} catch(NumberFormatException e) {
				System.out.println("Illegal input");
			}
		}
	}
	
	/**
	 * This method prepare the supermarket.
	 */
	private void prepareSupermarket() {
		getItems();
		getRules();
	}
	
	/**
	 * This method creates items for the supermarket.
	 */
	private void getItems() {
	      Item i1 = new Item(1, "wine", 5);
	      Item i2 = new Item(2, "Butter", 1.0);
	      Item i3 = new Item(3, "Grapes", 0.3);
	      Item i4 = new Item(4, "Bread", 2.0);
	      Item i5 = new Item(5, "Cheese", 2.5);
	      Item i6 = new Item(6, "Olives", 2.3);
	      Item i7 = new Item(7, "Gurkins", 2.1);
	      items.put(i1.getId(), i1);
	      items.put(i2.getId(), i2);
	      items.put(i3.getId(), i3);
	      items.put(i4.getId(), i4);
	      items.put(i5.getId(), i5);
	      items.put(i6.getId(), i6);
	      items.put(i7.getId(), i7);
	}
	
	/**
	 * this method creates a number of rule to be used by the supermarket
	 */
	public void getRules() {
		Rule r1 = new BuyGetFreeRule("Buy 3 pay for 2", 3, 2);
		r1.addItem(items.get((long) 2));
		Rule r2 = new NForSpecialRule("Buy 2 for 8", 2, 20);
		r2.addItem(items.get((long) 1));
		Rule r3 = new CheapestFromRule("Buy 3 items and get the cheapest one for free", 3, 1);
		r3.addItem(items.get((long) 3));
		r3.addItem(items.get((long) 4));
		r3.addItem(items.get((long) 5));
		Rule r4 = new GetFreeItemRule("Buy 2 Olives get Gurkins for free", 2, 1, items.get((long)7));
		r4.addItem(items.get((long) 6));
		rules.add(r1);
		rules.add(r2);
		rules.add(r3);
		rules.add(r4);
		rules.forEach(x -> checkout.addRule(x));
	}
}
