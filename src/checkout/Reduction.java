package checkout;

import checkout.rules.Rule;
import trading.Item;

/**
 * Created by nshoef on 03/06/2017.
 * Reduction is a wrap object to store the information about a reduction. It produced by applying a rule on a basket.
 * Reduction contain a nun null rule which created the reduction, a nun null item in reduction, and the sum to reduce.
 */
public class Reduction {
    private final Rule RULE;
    private final Item ITEM;
    private final double SUM;

    public Reduction(Rule rule, Item item, double sum) {
    	if(rule==null || item==null) throw new IllegalArgumentException("parameters can not be null");
        RULE = rule;
        ITEM = item;
        SUM = sum;
    }

    public Rule getRule() { return RULE; }
    public Item getItem() { return  ITEM; }
    public double getSum() { return SUM; }
    
    
    
    public String toString() {
    	return getItem()+" "+getRule()+" "+getSum(); 	
    }
    
    public int HashCode() {
    	int hashValue = 7;
    	hashValue = 31 * hashValue + RULE.hashCode();
    	hashValue = 31 * hashValue + ITEM.hashcode();
    	hashValue = 31 * hashValue + Double.hashCode(SUM);
    	return hashValue;
    	
    }

}
