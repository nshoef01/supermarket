package checkout;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import checkout.rules.Rule;
import trading.Basket;
import trading.Item;

/**
 * Created by nshoef on 02/06/2017.
 * Receip is an checked out basket, ready to be paid.
 * A receip can not be changed, if the basket is change, a new receip should be produce.
 */
public class Receip {
    private final Basket BASKET;
    private final List<Reduction> REDUCTIONS = new ArrayList<>();
    private double totalBeforeReduction = 0;
    private double totalAfterReduction = 0;

    /**
     * The constructor is building the reciecp from the given parameters
     * @param basket a ready basket to produce a reciep from
     * @param rules rules to apply on the basket.
     */
    public Receip(Basket basket, Set<Rule> rules) {

        BASKET = basket;

        calculateReductions(rules);

        calculateTotalBefore();

        calculateTotalAfter();
    }

    /**
     * Calculate the reduction for the given basket by applying the given rule on them.
     * @param rules
     */
    private void calculateReductions(Set<Rule> rules) {
        for(Rule rule :rules) {
            List<Reduction> reductions = rule.applyOn(BASKET);
            REDUCTIONS.addAll(reductions);
        }
    }

    /**
     * calculate the total amount to pay before any reductions.
     */
    private void calculateTotalBefore() {
        for(Item item : BASKET.getItems().keySet()) {
            totalBeforeReduction += (BASKET.getItems().get(item)*item.getPrice());
        }
    }

    /**
     * Calculate the total amount to pay after applying the reductions.
     */
    private void calculateTotalAfter() {
        double reductionSum = 0;
        for(Reduction reduction : REDUCTIONS) {
            reductionSum += reduction.getSum();
        }
        totalAfterReduction = totalBeforeReduction-reductionSum;
    }

    public Basket getBasket() {
        return BASKET;
    }

    public List<Reduction> getReductions() {
        return REDUCTIONS;
    }

    public double getTotalBeforeReduction() {
        return totalBeforeReduction;
    }

    public double getTotalAfterReduction() {
        return totalAfterReduction;
    }

    public String toString() {
        return "Basket: [" + BASKET.toString() + "], Reductions: [" + REDUCTIONS + "], " +
                "totalBeforeReduction: " + totalBeforeReduction + ", totalAfterReduction: " + totalAfterReduction;
    }

    public int hashCode() {
        int hashValue = 7;
        hashValue = 31 * hashValue+BASKET.hashCode();
        hashValue = 31 * hashValue+REDUCTIONS.hashCode();
        return hashValue;
    }

}
