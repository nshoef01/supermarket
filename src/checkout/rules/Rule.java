package checkout.rules;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import checkout.Reduction;
import trading.Basket;
import trading.Item;

/**
 * Created by nshoef on 02/06/2017.
 * A rule is a special offer which can be apply on a basket of items or on specific items.
 * By impementing the applyOn method each impementation of this class is apply a different offer on the basket/items.
 * The rule is being applyed to the items which are on the list.
 * The applyOn method returns a
 */
public abstract class Rule {
    private Set<Item> items = new HashSet<>();
    String name;

    public Rule(String name){
        this.name = name;
    }

    public Rule(String name, Set<Item> items) {
        this(name);
        this.items = items;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void addItems(Set<Item> items) {
        this.items.addAll(items);
    }

    public Set<Item> getItems() {
        return items;
    }

    public String getName() {
        return name;
    }

    /**
     * To be implemented according to the logic of a spesific rule.
     * @param basket the basket to apply the rule on.
     * @return a list of Reduction object, each contain the rule which gives the reduction(this),
     * the item in reduction, and the reduction sum.
     */
    public abstract List<Reduction> applyOn(Basket basket);


    public int hashCode() {
        int hashValue = 11;
        hashValue = 31 * hashValue+items.hashCode();
        hashValue = 31 * hashValue+name.hashCode();
        return hashValue;
    }
    
    public String toString() {
    	return name;
	}
}
