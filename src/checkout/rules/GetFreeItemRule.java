package checkout.rules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import checkout.Reduction;
import trading.Basket;
import trading.Item;

/**
 * Created by nshoef on 05/06/2017.
 * This rule implement an offer of buy x get y for free.
 */
public class GetFreeItemRule extends Rule {
	private int buy;
	private int free;
	private Item freeItem;
	    
	/**
	 * 
	 * @param name a miningful name of the rule
	 * @param buy the number of item needed to be bought in order to get free item/s.
	 * @param free the number of free item for every number of paid items.
	 * @param freeItem the tree item
	 */
	public GetFreeItemRule(String name, int buy, int free, Item freeItem) {
		super(name);
	    if(buy<1) throw new IllegalArgumentException("buy num of items must be 2 or greater");
	    this.buy = buy;
	    if(free<1) throw new IllegalArgumentException("free num of items must be 2 or greater");
	    this.free = free;
	    if(freeItem == null) throw new IllegalArgumentException("Free item can not be null");
	    this.freeItem = freeItem;
	}

	@Override
	public List<Reduction> applyOn(Basket basket) {
        List<Reduction> reductions = new ArrayList<>();
        Set<Item> items = new HashSet<>();
        items.addAll(basket.getItems().keySet());
        for(Item item : items) {
        	if(this.getItems().contains(item)) {
        		int quantity = basket.getItems().get(item);
        		if(quantity >= buy) {
        			int numOfFree = (quantity/buy)*free;
        			for(int i=0; i<numOfFree; i++) {
        				basket.addItem(freeItem, free);
        				reductions.add(new Reduction(this, freeItem, freeItem.getPrice()));
        			}	
        		}	
        	}
        }
        return reductions;
	}

}
