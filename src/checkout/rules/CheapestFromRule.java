package checkout.rules;

import java.util.ArrayList;
import java.util.List;

import checkout.Reduction;
import trading.Basket;
import trading.Item;

/**
 * Created by nshoef on 05/06/2017.
 * This rule implement an offer of buy x get the y cheapest items for free.
 */
public class CheapestFromRule extends Rule{
    private int buy;
    private int free;

    /**
     * @param name a miningful name of the rule
     * @param buy the number of item needed to be bought in order to get free ones.
     * @param free the number of free item for every number of paid items.
     */
    public CheapestFromRule(String name, int buy, int free) {
        super(name);
        if(buy<2) throw new IllegalArgumentException("buy num of items must be 1 or greater");
        this.buy = buy;
        if(free<1) throw new IllegalArgumentException("free num of items must be 1 or greater");
        this.free = free;
    }

    @Override
    public List<Reduction> applyOn(Basket basket) {
        List<Reduction> reductions = new ArrayList<>();
        List<Item> allItems = new ArrayList<>();
        for(Item item : basket.getItems().keySet()) {
            if(this.getItems().contains(item)) {
                for(int i=0; i<basket.getItems().get(item); i++ ) {
                    allItems.add(item);
                }
            }
        }
        //sort the item from most cheap to most expensive
        allItems.sort((o1, o2) -> o1.getPrice() - o2.getPrice() > 0? 1 : (o1.getPrice() - o2.getPrice() < 0?  -1 : 0)); 

        int numOfdeals = allItems.size() / buy;
        int marker = allItems.size();
        for(int i=0; i<numOfdeals; i++) {
            marker -= buy;
            for(int j=0; j<free; j++) {
                Item freeItem = allItems.get(marker+j);
                reductions.add(new Reduction(this, freeItem, freeItem.getPrice()));
            }
        }

        return reductions;
    }
}
