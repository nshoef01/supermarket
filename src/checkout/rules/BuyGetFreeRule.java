package checkout.rules;

import java.util.ArrayList;
import java.util.List;

import checkout.Reduction;
import trading.Basket;
import trading.Item;

public class BuyGetFreeRule extends Rule {
    private int buy;
    private int free;

    /**
     * @param name a minigfull name of the rule (e.g buy2get1)
     * @param buy the number of identical items needed to be paid to get a free item.
     * @param free the number of item to get for free for every buy items.
     */
    public BuyGetFreeRule(String name, int buy, int free) {
        super(name);
        if(buy<1) throw new IllegalArgumentException("buy must be greater then 0");
        this.buy = buy;
        if(free<1) throw new IllegalArgumentException("free must be greater then 0");
        this.free = free;
    }

    @Override
    public List<Reduction> applyOn(Basket basket) {
        List<Reduction> reductions = new ArrayList<>();
        for(Item item : basket.getItems().keySet()) {
            if(this.getItems().contains(item)) {
                int quantity = basket.getItems().get(item);
                if(quantity > buy) {
                    int numOfFree = free*(quantity/(buy+free));
                    int remiders = quantity%(buy+free);
                    if(remiders>buy) {
                        numOfFree += remiders-buy;
                    }
                    double totalReduction = numOfFree*item.getPrice();
                    reductions.add(new Reduction(this, item, totalReduction));
                }
            }
        }
        return reductions;
    }
	

}
