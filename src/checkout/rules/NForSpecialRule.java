package checkout.rules;

import java.util.ArrayList;
import java.util.List;

import checkout.Reduction;
import trading.Basket;
import trading.Item;

/**
 * Created by nshoef on 04/06/2017.
 * This class implemet a rule which gives a percent discount for each number of items.
 * That can be use for offers such as buy 1 get second in 50% discount (that apply 25% on each of the 2).
 * For example: if the offer is buy 3 get the forth in 50% then numOfItems will be 4 and the percentDiscount will be 12.5(50/4=12.5)
 * It can also be use to apply a general discount by placing 1 on the numOfItems and the requiered  discount on percentDiscount;
 */
public class NForSpecialRule extends Rule {
    private int numOfItems;
    private double percentDiscount;


    /**
     *
     * @param name a miningful name of the rule
     * @param numOfItems number of items to apply the rule
     * @param percentDiscount recent discount to apply on each item.
     */
    public NForSpecialRule(String name, int numOfItems, double percentDiscount) {
        super(name);
        if(numOfItems<1) throw new IllegalArgumentException("numOfItem must be at least 1");
        this.numOfItems = numOfItems;
        if(percentDiscount<0 || percentDiscount>100) throw new IllegalArgumentException("discount must be between 0 to 100");
        this.percentDiscount = percentDiscount;
    }

    @Override
    public List<Reduction> applyOn(Basket basket) {
        List<Reduction> reductions = new ArrayList<>();
        for(Item item : basket.getItems().keySet()){
            if(this.getItems().contains(item)) {
                int quantity = basket.getItems().get(item);
                if(quantity >= numOfItems) {
                    int numOfDiscounts = quantity/numOfItems;
                    double discountPerItem = (percentDiscount/100) * item.getPrice();
                    double totalReduction = numOfDiscounts *discountPerItem * numOfItems;
                    if(totalReduction>0) {
                        reductions.add(new Reduction(this, item, totalReduction));
                    }
                }
            }
        }
        return reductions;
    }
}
