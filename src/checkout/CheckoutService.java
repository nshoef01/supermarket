package checkout;

import java.util.HashSet;
import java.util.Set;

import checkout.rules.Rule;
import trading.Basket;

/**
 * @author nshoef
 * 
 */
public class CheckoutService {
	private final Set<Rule> rules = new HashSet<>();
	
	
	public boolean addRule(Rule rule) { return rules.add(rule); }

	public boolean removeRule(Rule rule) { return rules.remove(rule); }
	
	public Receip checkout(Basket basket) { return new Receip(basket, rules); }
	
	public Receip checkout(Basket basket, Set<Rule> rules) { return new Receip(basket, rules); }

}
